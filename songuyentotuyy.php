<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form method="POST">
        <h3>Số nguyên tố cần in ra :</h3>
        <input type="text" placeholder="Nhập số" name="amount">
        <input type="submit" value="In">
    </form>
    <?php
    function isPrimeNumber($n)
    {
        if ($n < 2) {
            return false;
        }
        for ($i = 2; $i <= sqrt($n); $i++) {
            if ($n % $i == 0) {
                return false;
            }
        }
        return true;
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $amount = $_POST["amount"];
        $result = $amount . ": ";
        $count = 0;
        $i = 2;
        while ($count < $amount) {
            if (isPrimeNumber($i)) {
                $result .=  $i . " ";
                $count ++;
            }
            $i++;
        }
        echo $result;
    }

    ?>
</body>
</html>