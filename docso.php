<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        input[type=text] {
            width: 300px;
            font-size: 16px;
            border: 2px solid #ccc;
            border-radius: 4px;
            padding: 12px 10px 12px 10px;
        }

        #submit {
            border-radius: 2px;
            padding: 10px 32px;
            font-size: 16px;
        }
    </style>
</head>

<body>
    <br />
    <h3>Ứng dụng đọc số thành chữ</h3>
    <form method="post">
        <input type="text" name="convert" placeholder="Nhập số cần đọc" />
        <input type="submit" id="submit" value="Đọc" />
    </form>
    <?php if ($_SERVER["REQUEST_METHOD"] == "POST") {
        var_dump($_POST["convert"]);
        $number = $_POST["convert"];
        switch ($number) {
            case 0:
                echo 'Zero';
                break;
            case 1:
                echo 'One';
                break;
            case 2:
                echo 'Two';
                break;
            case 3:
                echo 'Three';
                break;
            case 4:
                echo 'Four';
                break;
            case 5:
                echo 'Five';
                break;
            case 6:
                echo 'Six';
                break;
            case 7:
                echo 'Seven';
                break;
            case 8:
                echo 'Eight';
                break;
            case 9:
                echo 'Nine';
                break;
            case 10:
                echo 'Ten';
                break;
            case 11:
                echo 'One Teen';
                break;
            case 12:
                echo 'Two Teen';
                break;
            case 13:
                echo 'Three Teen';
                break;
            case 14:
                echo 'Four Teen';
                break;
            case 15:
                echo 'Five Teen';
                break;
            case 16:
                echo 'Six Teen';
                break;
            case 17:
                echo 'Seven Teen';
                break;
            case 18:
                echo 'Eight Teen';
                break;
            case 19:
                echo 'Nine Teen';
                break;
            default:
                echo 'out of ability';
                break;
        }
    }

    ?>

</body>

</html>