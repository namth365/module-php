<?php

class Patient
{
    public $name;
    public $code;

    public function __construct($name, $code)
    {
        $this->name = $name;
        $this->code = $code;
    }
}
class Queue
{
    public $patientList;
    public $count;

    public function __construct()
    {
        $this->patientList = [];
        $this->count = 0;
    }
    public function enqueue($name, $code)
    {
        $this->patientList[] = new Patient($name, $code);
        $this->count++;
    }
    public function dequeue()
    {
        $minCode = $this->patientList[0]->code;
        for ($i = 1; $i < $this->count; $i++) {
            if ($minCode > $this->patientList[$i]->code) {
                $minCode = $this->patientList[$i]->code;
            }
        }
        for ($i = 0; $i < $this->count; $i++) {
            if ($this->patientList[$i]->code === $minCode) {
                $tmp = $this->patientList[$i]->name;
                for ($j = $i; $j < $this->count - 1; $j++) {
                    $this->patientList[$j] = $this->patientList[$j + 1];
                }
                array_pop($this->patientList);
                $this->count--;
                return $tmp;
            }
        }
    }
    public function toString()
    {
        $str = "";
        for ($i = 0; $i < $this->count; $i++) {
            $str .= $this->patientList[$i]->name . " - ";
            $str .= $this->patientList[$i]->code . "   => ";
        }
        return $str;
    }
}

$patients = new Queue;
$patients->enqueue('Smith', 5);
$patients->enqueue('Jones', 4);
$patients->enqueue('Fehrenbach', 6);
$patients->enqueue('Brown', 1);
$patients->enqueue('Ingram', 1);
echo "<br> Nhập vào 5 bệnh nhân: <br>";
echo $patients->toString();
echo "<br> Khám người đầu tiên: <br>";
echo $patients->dequeue();
echo "<br> Các bệnh nhân còn lại:<br>";
echo $patients->toString();
echo "<br> Khám người thứ 2: <br>";
echo $patients->dequeue();
echo "<br> Các bệnh nhân còn lại:<br>";
echo $patients->toString();
echo "<br> Khám người thứ 3: <br>";
echo $patients->dequeue();
echo "<br> Các bệnh nhân còn lại:<br>";
echo $patients->toString();
echo "<br> Khám người thứ 4: <br>";
echo $patients->dequeue();
echo "<br> Các bệnh nhân còn lại:<br>";
echo $patients->toString();
