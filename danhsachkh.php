<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Heros Girl</title>
    <style >
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
    </style>
</head>

<body> 
    <?php
    $customerList = [
        "1" => [
            "ten" => "Ishar",
            "ngaysinh" => "2010-08-20",
            "diachi" => "Xứ Sở Thần Tiên",
            "anh" => "img/ish.jpg"
        ],
        "2" => [
            "ten" => "Capheny",
            "ngaysinh" => "2000-08-20",
            "diachi" => "Học Viện Âm Nhạc",
            "anh" => "img/cap.jpg"
        ],
        "3" => [
            "ten" => "Annette",
            "ngaysinh" => "1999-08-21",
            "diachi" => "Học Viện Âm Nhạc",
            "anh" => "img/annet.jpg"
        ],
        "4" => [
            "ten" => "Liliana",
            "ngaysinh" => "1996-08-22",
            "diachi" => "Học Viện Âm Nhạc",
            "anh" => "img/lili.jpg"
        ],
        "5" => [
            "ten" => "Sephera",
            "ngaysinh" => "1993-08-17",
            "diachi" => "Học Viện Âm Nhạc",
            "anh" => "img/sep.jpg"
        ]
    ];
    ?>
    <table>
        <caption>
            <h1> DANH SÁCH HỌC VIÊN
            </h1>
        </caption>
        <thead>
            <tr>
                <th>STT</th>
                <th>TÊN</th>
                <th>NĂM SINH</th>
                <th>ĐẾN TỪ</th>
                <th>ẢNH</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customerList as $key => $value) : ?>
                <tr>
                    <td><?php echo $key ?></td>
                    <td><?php echo $value['ten'] ?></td>
                    <td><?php echo $value['ngaysinh'] ?></td>
                    <td><?php echo $value['diachi'] ?></td>
                    <td><img src="<?php echo $value['anh'] ?>" alt="" width="200"></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
   
 </body>
 

</html>