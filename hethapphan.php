<?php

class Stack
{
    protected $stack;
    protected $limit;
    public function __construct($limit)
    {
        $this->stack = [];
        $this->limit = $limit;
    }
    public function push($data)
    {
        $len = count($this->stack);
        if ($len < $this->limit) {
            array_unshift($this->stack, $data);
        } else {
            echo "Ngăn xếp đầy!";
        }
    }
    public function pop()
    {
        $len = count($this->stack);

        if ($len > 0) {
            $dataPop = $this->stack[0];
            array_shift($this->stack);
            return $dataPop;
        } else {
            return "Ngăn xếp rỗng!";
        }
    }
    public function top()
    {
        $len = count($this->stack);
        if ($len > 0) {
            return $this->stack[0];
        } else {
            return "Ngăn xếp rỗng!";
        }
    }
    public function isEmpty()
    {
        $len = count($this->stack);
        if ($len > 0) {
            return false;
        } else {
            return true;
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $myStack = new Stack(30);
    $count = 0;
    $dec = $_POST['dec'];

    while ($dec / 2 > 0) {
        $myStack->push($dec % 2);
        $count++;
        $dec = floor($dec / 2);
    }
    $str = "";
    for ($i = 0; $i < $count; $i++) {
        $str .=  $myStack->pop();
    }
    echo $str;
}
