<?php
class User
{
    protected string $name = "";
    protected string $email = "";
    public int $role = 1;
    public function __construct()
    {
    }
    public function isAdmin()
    {
        if ($this->role == 1) {
            return "Admin";
        } elseif ($this->role == 2) {
            return "user";
        }
    }
    public function getInfo()
    {
        return " <br>Tên: " . $this->name . " <br>Email : " . $this->email . " <br>Role: " . $this->isAdmin();
    }
    public function setName($name): void
    {
        $this->name = $name;
    }
    public function setEmail($email): void
    {
        $this->email = $email;
    }
    public function setRole($role): void
    {
        $this->role = $role;
    }
}
$user1 = new User;
$user1->setName('user1');
$user1->setEmail('user1@gmail.com');
$user1->setRole(1);
echo $user1->getInfo();
