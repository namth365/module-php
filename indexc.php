<?php
class Circle
{
    public $radius;
    public $color;
    public function __construct($color, $radius)
    {
        $this->radius = $radius;
        $this->color = $color;
    }
    public function get($propertyName)
    {
        return $this->$propertyName;
    }
    public function set($propertyName, $propertyValue)
    {
        $this->$propertyName = $propertyValue;
    }
    public function calculatePerimeter()
    {
        return pi() * $this->radius * 2;
    }
    public function calculateArea()
    {
        return pi() * pow($this->radius, 2);
    }
}
class Cylinder extends Circle
{
    public $height;
    public function __construct($color, $radius, $height)
    {
        $this->color = $color;
        $this->radius = $radius;
        $this->height = $height;
    }
    public function calculateArea()
    {
        return parent::calculateArea() * 2 + $this->height * parent::calculatePerimeter();
    }
    public function calculateVolume()
    {
        return parent::calculateArea() * $this->height;
    }
}
$circle1 = new Circle('red', 5);

echo "Color's cirlce :" . $circle1->get('color') . "<br />";
echo "Radius's cirlce :" . $circle1->get('radius') . "<br />";
echo "Area's circle :" . $circle1->calculateArea() . "<br />";
echo "Perimeter's circle :" . $circle1->calculatePerimeter() . "<br />";
$cylinder1 = new Cylinder('green', 4, 8);
echo "Color's cylinder :" . $cylinder1->get('color') . "<br />";
echo "Radius's cylinder :" . $cylinder1->get('radius') . "<br />";
echo "Height's cylinder :" . $cylinder1->get('height') . "<br />";
echo "Area's circle :" . $cylinder1->calculateArea() . "<br />";
echo "Perimeter's circle :" . $cylinder1->calculateVolume() . "<br />";
