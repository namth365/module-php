<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #content {
            width: 450px;
            margin: 0 auto;
            padding: 0px 20px 20px;
            background: yellow;
            border: 2px;
        }

        h1 {
            color: ;
        }

        label {
            width: 10em;
            padding-right: 1em;
            float: left;
        }

        #data input {
            float: left;
            width: 15em;
            margin-bottom: .5em;
        }

        #buttons input {
            float: left;
            margin-bottom: .5em;
        }

        br {
            clear: left;
        }
    </style>
</head>

<body>
    <div id="content">
        <h1>Future Value Calculator</h1>
        <p class="error">Tính Lợi Nhuận Đầu Tư</p>

        <form action="FuVaCa.php" method="POST">
            <div id="data">
                <label>Số tiền đầu tư:</label>
                <input type="text" name="investment" value="0" /><br />

                <label>Lãi suất:</label>
                <input type="text" name="rate" value="0" /><br />

                <label>Số năm:</label>
                <input type="text" name="years" value="0" />
            </div>
            <div id="buttons">
                <label>&nbsp;</label>
                <input type="submit" value="Tính Xem" /><br />
            </div>
        </form>
    </div>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $investment = $_POST["investment"];
        $rate = $_POST["rate"] / 100;
        $years = $_POST["years"];
        $result = $investment;
        for ($i = 0; $i < $years; $i++) {
            $result += $investment * $rate;
        }
        echo "<div id='content'>
                <h1>Sau $years Năm</h1>
                <form>
                    <div id='data'>
                     <p>Số Tiền Thu Được: $" . $result . "</p>
                    </div>
                </form>            
            </div>";
    }
    ?>
</body>

</html>