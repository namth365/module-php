<?php

$list = [2, 3, 2, 5, 6, 1, -2, 3, 14, 12];

bubbleSort($list);

for ($i = 0; $i < count($list); $i++) {
    echo $list[$i] . " / ";
}

function bubbleSort(&$list)
{
    $count = count($list);
    for ($i = 0; $i < $count - 1; $i++) {
        $check = true;
        for ($j = 0; $j < $count - $i - 1; $j++) {
            if ($list[$j] < $list[$j + 1]) {
                $tmp = $list[$j];
                $list[$j] = $list[$j + 1];
                $list[$j + 1] = $tmp;
                $check = false;
            }
        }
        if ($check) {
            break;
        }
    }
}
