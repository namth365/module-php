<?php

class Stack
{
    protected $stack;
    protected $limit;
    public function __construct($limit)
    {
        $this->stack = [];
        $this->limit = $limit;
    }
    public function push($data)
    {
        $len = count($this->stack);
        if ($len < $this->limit) {
            array_unshift($this->stack, $data);
        } else {
            echo "Ngăn xếp đầy!";
        }
    }
    public function pop()
    {
        $len = count($this->stack);

        if ($len > 0) {
            $dataPop = $this->stack[0];
            array_shift($this->stack);
            return $dataPop;
        } else {
            return "Ngăn xếp rỗng!";
        }
    }
}

class Queue
{
    public $queueList;
    public $count;

    public function __construct()
    {
        $this->queueList = [];
        $this->count = 0;
    }
    public function enqueue($data)
    {
        $this->queueList[] = $data;
        $this->count++;
    }
    public function dequeue()
    {
        $data = $this->queueList[0];
        array_shift($this->queueList);
        $this->count--;
        return $data;
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $str = $_POST['string'];
    echo "<br> Bạn đã nhập: <br>";
    echo $str . "<br>";
    $len = strlen($str);
    $queue = new Queue;
    $stack = new Stack($len);
    for ($i = 0; $i < $len; $i++) {
        $queue->enqueue($str[$i]);
        $stack->push($str[$i]);
    }
    $check = true;
    for ($i = 0; $i < $len; $i++) {
        if ($queue->dequeue() !== $stack->pop()) {
            $check = false;
        }
    }
    if ($check) {
        echo "<br> Chuỗi của bạn đối xứng! <br>";
    } else {
        echo "<br> Chuỗi của bạn không đối xứng! <br>";
    }
}
