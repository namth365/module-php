<?php
class Node {
    public $value;
    public $next;
    public function __construct($value)
    {
    $this->value = $value;
    $this->next = NULL;    
    }
}
class Queue
{
    public $front;
    public $back;
    public $count;

    public function __construct()
    {
        $this->front = NULL;
        $this->back = NULL;
        $this->count = 0;
    }
    public function printQueue(){
        $current = $this->back;
        $dataArr = [];
        for($i = 0; $i < $this->count; $i++){
           $dataArr[] = $current->value;
           $current = $current->next;
        }
        return $dataArr;
    }
    public function isEmpty()
    {
        if ($this->front === NULL) {
            return true;
        } else {
            return false;
        }
    }
    public function enqueue($value)
    {
        if ($this->count === 0) {
            $new = new Node($value);
            $this->count++;
            $this->front = $new;
            $this->back = $new;
        } else {
            $new = new Node($value);
            $this->count++;
            $new->next = $this->back;
            $this->back = $new;
        }
    }
    public function dequeue()
    {
        if ($this->count === 0) {
            echo "<br> Hàng đợi rỗng, không có gì để lấy! <br>";
        } else {
            if ($this->count === 1) {
                $tmp = $this->front->value;
                $this->front->value = NULL;
                $this->front = NULL;
                $this->back = NULL;
                $this->count = 0;
                return $tmp;
            } else {
                $current = $this->back;
                for ($i = 1; $i < $this->count - 1; $i++) {
                    $current = $current->next;
                }
                $tmp = $this->front->value;
                $this->front->value = NULL;
                $this->front = $current;
                $this->count--;
                return $tmp;
            }
        }
    }
}
$queue1 = new Queue;
echo "<br> Kiểm tra hàng đợi sau khi khởi tạo: <br>";
if($queue1->isEmpty()){
    echo"<br> Hàng đợi rỗng <br>";
}
else{
    echo "<br> Hàng đợi có giá trị <br>";
}
echo "<br> Nhập vào 3 phần tử 15 16 17 <br>";
$queue1->enqueue(15);
$queue1->enqueue(16);
$queue1->enqueue(17);
print_r($queue1->printQueue());
echo "<br> Lấy ra 2 phần tử <br>";
echo "<br>".$queue1->dequeue()."<br>";
echo "<br>".$queue1->dequeue()."<br>";
echo "<br> Còn lại: <br>";
print_r($queue1->printQueue());
echo "<br> Kiểm tra rỗng? <br>";
if($queue1->isEmpty()){
    echo"<br> Hàng đợi rỗng <br>";
}
else{
    echo "<br> Hàng đợi có $queue1->count giá trị <br>";
}
echo "<br> Nhập vào 2 phần tử 18 19 <br>";
$queue1->enqueue(18);
$queue1->enqueue(19);
print_r($queue1->printQueue());
echo "<br> Lấy ra 3 phần tử <br>";
echo "<br>".$queue1->dequeue()."<br>";
echo "<br>".$queue1->dequeue()."<br>";
echo "<br>".$queue1->dequeue()."<br>";
echo "<br> Kiểm tra rỗng? <br>";
if($queue1->isEmpty()){
    echo"<br> Hàng đợi rỗng <br>";
}
else{
    echo "<br> Hàng đợi có $queue1->count giá trị <br>";
}