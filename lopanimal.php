<?php
abstract class Animal
{
    abstract public function makeSound();
}

class Tiger extends Animal
{
    public function makeSound(): string
    {
        return "Tiger: roarr!!";
    }
}

class Chicken extends Animal
{
    public function makeSound(): string
    {
        return "Chicken: cluck-cluck!";
    }
}
echo ("...Animal");
$animals[0] = new Tiger();
$animals[1] = new Chicken();
foreach ($animals as $animal) {
    echo $animal->makeSound() . '<br>';
}

