<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #content {
            width: 450px;
            margin: 0 auto;
            padding: 0px 20px 20px;
            background: yellow;
            border: 2px;
        }

        h1 {
            color: red;
        }

        label {
            width: 10em;
            padding-right: 1em;
            float: left;
        }

        #data input {
            float: left;
            width: 15em;
            margin-bottom: .5em;
        }

        #buttons input {
            float: left;
            margin-bottom: .5em;
        }

        br {
            clear: left;
        }
    </style>
</head>

<body>
    <div id="content">
        <h1>Simple Calculator</h1>
        <form method="post" action="">
            <div id="data">
                <label>First Operand:</label>
                <input type="text" name="first_operand" /><br />

                <label>Bảng Tính:</label>
                <select name="operator">
                    <option value="addition" selected>Cộng</option>
                    <option value="subtraction">Trừ</option>
                    <option value="multiple">Nhân</option>
                    <option value="division">Chia</option>
                </select><br><br>

                <label>Second Operator:</label>
                <input type="text" name="second_operand" /><br />
            </div>
            <div id="buttons">
                <label>&nbsp;</label>
                <input type="submit" value="Calculate" />
            </div>
        </form>
    </div>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $first_Operand = $_POST["first_operand"];
        $select_Operator = $_POST["operator"];
        $second_Operand = $_POST["second_operand"];
        $result = 0;
        if ($select_Operator == "addition") {
            $result = $first_Operand + $second_Operand;
        } else if ($select_Operator == "subtraction") {
            $result = $first_Operand - $second_Operand;
        } else if ($select_Operator == "multiple") {
            $result = $first_Operand * $second_Operand;
        } else {
            if ($second_Operand == 0) {
                $result = "Error";
            } else {
                $result = $first_Operand / $second_Operand;
            }
        };
        echo "<h1 style='text-align:center;'>Kết Quả:" . $result . "</h1>";
    }
    ?>
</body>

</html>