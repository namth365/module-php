<?php
$list = [1, 9, 4.5, 6.6, 5.7, -4.5];

selectionSort($list);

for ($i = 0; $i < count($list); $i++) {
    echo $list[$i] . " / ";
}

function selectionSort(&$list)
{
    $count = count($list);
    for ($i = 0; $i < $count; $i++) {
        $min = $i;
        for ($j = $i + 1; $j < $count; $j++) {
            if ($list[$min] > $list[$j]) {
                $min = $j;
            }
        }
        if ($min !== $i) {
            $tmp = $list[$min];
            $list[$min] = $list[$i];
            $list[$i] = $tmp;
        }
    }
}
