<?php
$list = [5, -4, 3, 7, 2, 1, 0, 8, 9, 2];

insertionSort($list);

for ($i = 0; $i < count($list); $i++) {
    echo $list[$i] . "  ";
}

function insertionSort(&$list)
{
    $count = count($list);
    for ($i = 0; $i < $count; $i++) {
        $value = $list[$i];
        for ($j = $i - 1; $j >= 0; $j--) {
            if ($value < $list[$j]) {
                $list[$j + 1] = $list[$j];
                $list[$j] = $value;
            }
        }
    }
}
