<?php
class Point
{
    public float $x;
    public float $y;
    public function __construct(float $x, float $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): float
    {
        return $this->x;
    }
    public function  setX(float $x): void
    {
        $this->x = $x;
    }
    public function getY(): float
    {
        return $this->y;
    }
    public function setY(float $y): void
    {
        $this->y = $y;
    }
    public function setXY(float $x, float $y): void
    {
        $this->x = $x;
        $this->y = $y;
    }
    public function getXY()
    {
        return $this->x . ',' . $this->y;
    }
    public function toString()
    {
        return  "Array " . $this->x . ',' . $this->y;
    }
}

class MoveablePoint extends Point
{
    public $xSpeed;
    public $yspeed;
    public function __construct(float $x, float $y)
    {
        parent::__construct($x, $y);
        $this->xSpeed = $x;
        $this->ySpeed = $y;
    }
    public function getXSpeed() : float
    {
        return $this->xSpeed;
    }
    public function setXSpeed($xSpeed) :void
    {
        $this->xSpeed = $xSpeed;
    }
    public function getYSpeed() : float
    {
        return $this->ySpeed;
    }
    public function setYSpeed($ySpeed)  :void
    {
        $this->ySpeed = $ySpeed;
    }
    public function getSpeed()
    {
        return array($this->xSpeed, $this->ySpeed);
    }
    public function setSpeed($xSpeed, $ySpeed)
    {
        $this->xSpeed = $xSpeed;
        $this->ySpeed = $ySpeed;
    }
    public function toString()
    {
        return  "Array ". "($this->xSpeed,$this->ySpeed)";
    }
    public function move()
    {

        return $this->xSpeed. ",".$this->ySpeed ;
    }
}

$objPoint1 = new Point(2, 2);
echo "<br/>Point: " . $objPoint1->toString();

$objMovablePoint2 = new MoveablePoint(5, 10);
echo "<br/>MovablePoint: " . $objMovablePoint2->toString();
