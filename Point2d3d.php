<?php
class Point2D
{
    public float $x;
    public float $y;
    public function __construct(float $x, float $y)
    {
        $this->x = $x;
        $this->y = $y;
    }
    public function getX(): float
    {
        return $this->x;
    }
    public function setX(float $x):void
    {
        $this->x = $x;
    }
    public function getY(): float
    {
        return $this->y;
    }
    public function setY(float $y) :void
    {
        $this->y = $y;
    }
    public function setXY(float $x, float $y):void
    {
        $this->x = $x;
        $this->y = $y;
    }
    public function getXY()
    {
        return $this->x.','. $this->y;
    }
    public function toString()
    {
        return  "Array " .$this->x .','. $this->y;
    }
}


class Point3D extends Point2D
{
    public float $z;
    public function __construct(float $x,float $y,float $z)
    {
        parent::__construct($x, $y);
        $this->z = $z;
    }
    public function getZ():float
    {
        return $this->z;
    }
    public function setZ(float $z):void
    {
        $this->z = $z;
    }
    public function setXYZ(float $x, float $y, float $z):void
    {
        parent::setXY($x,$y);
        $this->z=$z;
    }
    public function getXYZ()
    {
        return  $this->x.','. $this->y. ',' .$this->z;
    }
    public function toString()
    {
        return  "Array " .$this->x.',' .$this->y. ',' .$this->z;
    }
}

$point2d = new Point2D(3, 5);
echo $point2d->toString();
echo "<br/>";

$point3d = new Point3D(3, 5, 7);
echo $point3d->toString();

